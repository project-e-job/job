<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('gen_uuid')) {
	function gen_uuid(){
		$CI =& get_instance();
		return $CI->db->query('SELECT UUID() AS uuid')->row()->uuid;	
	}
	
}
if (!function_exists('upload_berkas')) {
	function upload_berkas($file_name, $folder, $input_name){
		$CI =& get_instance();
		$config['file_name'] = $file_name;
		$config['upload_path'] = 'http://192.168.108.11:800/jobpair/src/'.$folder;

		$config['allowed_types'] = 'jpg|png';
		$config['overwrite'] = TRUE;
		$config['max_size'] = 2048;

		$CI->load->library('upload',$config);
		$CI->upload->do_upload($input_name);
		return $CI->upload->data();
	}	
}	