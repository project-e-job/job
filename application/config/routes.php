<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'landing_page';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'auth';
$route['logout'] = 'auth/logout';

// route perusahaan
$route['regis_perusahaan'] = 'auth/registration_2';
$route['dashboard'] = 'perusahaan';
$route['profil_perusahaan'] = 'perusahaan/profil';
$route['edit_lowongan'] = 'perusahaan/lowongan';
// $route['upload_surat'] = '';


// route operator
$route['operator'] = 'operator';
$route['verif'] = 'operator/verif';
$route['holis1'] = 'operator/index2';
$route['holis2'] = 'operator/index3';
$route['holis3'] = 'operator/index';
$route['s_verif'] = 'operator/simpandata';


// route pelamar
$route['regis_pelamar'] = 'auth/regis_pelamar';
$route['profile'] = 'pelamar/profile';
$route['joblist'] = 'pelamar/joblist';
$route['kuisioner'] = 'pelamar/kuisioner';
$route['pendidikan'] = 'pelamar/pendidikan';
$route['data_didik'] = 'pelamar/data_didik';
$route['data_upload'] = 'pelamar/upload_ktp';
