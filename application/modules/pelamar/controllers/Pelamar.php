<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelamar extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(); 
        $this->url_api = 'http://192.168.108.11:800/jobpair';
    }

    public function index()
    {
        $data['role'] = '1';
        $data['title'] = 'Tolopani';
        $data['content_page'] = 'pelamar/dashboard_pelamar';
        echo Modules::run('template/loadTemplate', $data);
    }

    public function profile()
    {
        $kd = $this->session->userdata('kd');

        $data = array (
            'kd_user' => $kd
        );

        $encode = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_api."/pelamar/get_biodata");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encode);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $decode = json_decode($result, true);
        if (!$decode["error"]) {
            /*if ($decode["code"]==1) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email ini berhasil diregistrasi.</div>');
                redirect('login');
            }elseif ($decode["code"]==0) {
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Email ini sudah aktif</div>');
                redirect('regis_pelamar');
            }*/

            $data['biodata']=$decode['data'];
            $data['role'] = '1';
            $data['title'] = 'Tolopani';
            $data['content_page'] = 'pelamar/profile_pelamar';
            echo Modules::run('template/loadTemplate', $data);
        }

    }

    public function update_profile()
    {
        $kd = $this->session->userdata('kd');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $jk = $this->input->post('jk');
        $data = array (
            'kd_user' => $kd,
            'nama' => $nama,
            'alamat' => $alamat,
            'no_hp' => $no_hp,
            'jk' => $jk
        );

        $encode = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_api."/pelamar/update_biodata");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encode);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $decode = json_decode($result, true);

        redirect('profile');

    }

    public function upload_sertifikat()
    {
        $filename = $_SESSION['kd'];
        $folder = 'sertifikat';
        $input_name = 'sertifikat';
        // $this->load->helper('hupload');
        $dt = upload_berkas($filename,$folder,$input_name);
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
        exit();
    }

    public function upload_ktp()
    {
        $filename = $_SESSION['kd'];
        $folder = 'ktp';
        $input_name = 'ktp';
        // $this->load->helper('hupload');
        $dt = upload_berkas($filename,$folder,$input_name);
        echo "<pre>";
        print_r($dt);
        echo "</pre>";
        exit();


        /*if($response['errorCode'] == 200){
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $config['upload_path'] = './gambar_produk/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;

        $this->load->library('upload', $config);

        $encrypted_gambar = md5($params['orig_filename']);
        $params['orig_filename'] = $params['orig_filename'];
        $params['filename'] = $encrypted_gambar;
        $params['urlgambar']="http://192.168.108.11/rest_server/gambar_produk/$encrypted_gambar";
        $resp = $this->m_gambar_produk->create_data($params);

        if ($resp['errorCode'] == 200) {
        $stat = "SUCCESS";
        }else{
        $stat = "ERROR";
        }
        $count = array($resp);
        $jsonAr = array(
        "_meta" => array('status' => $stat,'count' => count($count)),
        "result" => $resp
        );
        json_output($resp['errorCode'],$jsonAr);
        }else{
        $respStatus = 201;
        $jsonAr = array(
        "_meta" => array('Status' => 'ERROR','count' => 1),
        "result" => array('errorCode' => 201,'userMessage' => 'Title & Author can\'t empty')
        );
        $resp = $jsonAr;
        json_output($respStatus,$resp);
        }

        redirect('profile');*/
    }

    public function joblist()
    {
        $data['role'] = '1';
        $data['title'] = 'LIst Job';
        $data['content_page'] = 'pelamar/joblist';
        echo Modules::run('template/loadTemplate', $data);
    }


    public function kuisioner()
    {
        $data['role'] = '1';
        $data['title'] = 'kuisioner';
        $data['content_page'] = 'pelamar/kuisioner';
        echo Modules::run('template/loadTemplate', $data);
    }

     public function pendidikan()
    {
         /*$kd = $this->input->get('kd');
         $data = array (
            'kd_user' => $kd
         )

        $encode = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_api."/pelamar/get_biodata");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encode);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $decode = json_decode($result, true);

        if (!$decode["error"]){
            tampilan
        }*/


        $data['role'] = '1';
        $data['title'] = 'Pendidikan';
        $data['content_page'] = 'pelamar/pendidikan_pelamar';
        echo Modules::run('template/loadTemplate', $data);
        
    }

     public function data_didik()
    {
        $data['role'] = '1';
        $data['title'] = 'Data Pendidikan';
        $data['content_page'] = 'pelamar/tambah_dt_didik';
        echo Modules::run('template/loadTemplate', $data);
    }


}
