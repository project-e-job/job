<br>
<div class="container" style="padding: 20px;">
	<form action="" method="POST">
		<div class="row">
			<div class="col">
				<p style="font-family: Poppins;"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Jenjang Pendidikan : </p>
				<div style="padding-top: 3px;">
					<div class="input-group mb-3">
						<select class="custom-select" name="jenjang">
							<option selected>Pilih Jenjang</option>
							<option value="1">SMP</option>
							<option value="2">SMA</option>
							<option value="3">SMK</option>
							<option value="4">D1</option>
							<option value="5">D2</option>
							<option value="6">D3</option>
							<option value="7">S1</option>
						</select>
					</div>
				</div>

				<p style="font-family: Poppins;"><i class="fa fa-university" aria-hidden="true"></i> Instansi : </p><input type="text" placeholder="Instansi" ><br>

				<p style="font-family: Poppins;"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Jurusan/Bidang Keahlian : </p><input type="text" id="autouser" placeholder="Jurusan" >

				<!-- Script -->

				<script type='text/javascript'>
					$(document).ready(function(){

						$( "#autouser" ).autocomplete({
							source: function( request, response ) {
				       // Fetch data
				       $.ajax({
				       	url: "<?=base_url()?>index.php/User/userList",
				       	type: 'post',
				       	dataType: "json",
				       	data: {
				       		search: request.term
				       	},
				       	success: function( data ) {
				       		response( data );
				       	}
				       });
				   },
				   select: function (event, ui) {
				       // Set selection
				       $('#autouser').val(ui.item.label); // display the selected text
				       return false;
				   },
				   focus: function(event, ui){
				   	$( "#autouser" ).val( ui.item.label );
				   	return false;
				   },
				});

					});
				</script>
			</div>

			<div class="col">
				<p style="font-family: Poppins;"><i class="fa fa-calendar" aria-hidden="true"></i> Terhitung Mulai Tanggal : </p>
				<div class="input-group mb-3" style="padding-top: 3px;">			
					<input type="text" class="form-control" name="datepicker" id="datepicker">
				</div>

				<p style="font-family: Poppins;"><i class="fa fa-calendar" aria-hidden="true"></i> Terhitung Selesai Tanggal : </p>
				<div class="input-group mb-3" style="padding-top: 3px;">
					<input type="date" class="form-control" name="tst">
				</div>

				<p style="font-family: Poppins;"><i class="fa fa-certificate" aria-hidden="true"></i> Ijazah : </p>
				<div style="padding-top: 20px;" class="input-group">
					<div class="custom-file">
						<input type="file" class="custom-file-input" name="ijazah" aria-describedby="inputGroupFileAddon04">
						<label class="custom-file-label" for="inputGroupFile04">Upload Ijazah</label>
					</div>
				</div>

				<input type="submit" name="simpan" value="Tambahkan" class="btn" style="background-color: #610655; font-family: Poppins; color: #fff">
			</div>
		</div>
	</form>	
</div>
<br><br><br>

<script type="text/javascript">
$(document).ready(function(){
	$("#datepicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years",
    autoclose:true //to close picker once year is selected
});
})
</script>
