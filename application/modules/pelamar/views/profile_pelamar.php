<h2 style="padding-top: 20px; text-align: center;"><i class="icofont-thief"></i>Profile Anda</h2>
  <div class="container">
    <form method="POST" action="<?= base_url('pelamar/update_profile'); ?>">

      <div class="form-group">
        <label for="name"><i class="fa fa-id-card" aria-hidden="true"></i></label>
        <input type="text" name="nik" placeholder="NIK" disabled value="<?= $biodata['nik'] ?>" />
      </div>

      <div class="form-group">
        <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
        <input type="text" name="nama" placeholder="Nama Lengkap" value="<?= $biodata['nama'] ?>" />
      </div>

      <div class="form-group">
        <label for="hp"><i class="fa fa-phone" aria-hidden="true"></i></i></label>
        <input type="text" name="no_hp" placeholder="Nomor Handphone" value="<?= $biodata['no_hp'] ?>" />
      </div>

      <div class="form-group">
        <label for="alamat"><i class="fa fa-map-marker" aria-hidden="true"></i></label>
        <input type="text" name="alamat" id="name" placeholder="Alamat" value="<?= $biodata['alamat'] ?>" />
      </div>

      <div>
        <h6><i class="fa fa-venus-mars" aria-hidden="true"></i> &nbsp; Jenis Kelamin</h6>
      </div>

      <div class="custom-control custom-radio custom-control-inline" style="padding-top: 10px;">
        <input type="radio" id="customRadioInline1" name="jk" value="Laki - laki" class="custom-control-input" <?php if ($biodata['jk']=='Laki - laki') {
              echo "checked";
            } ?>>
        <label class="custom-control-label" for="customRadioInline1">Laki-Laki</label>
      </div>

      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline2" name="jk" value="Perempuan" class="custom-control-input" <?php if ($biodata['jk']=='Perempuan') {
            echo "checked";
          } ?>>
        <label class="custom-control-label" for="customRadioInline2">Perempuan</label>
      </div>

      <div class="form-group">
        <label for="email"><i class="zmdi zmdi-email"></i></label>
        <input type="email" name="email" id="email" placeholder="Email" value="<?= $biodata['email'] ?>" />
      </div>

      <div class="form-group">
        <label for="pass"><i class="zmdi zmdi-lock"></i></label>
        <input type="password" name="pass" id="pass" placeholder="Password"/>
      </div>

      <div class="form-group">
        <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
        <input type="password" name="re_pass" id="re_pass" placeholder="Re-password"/>
      </div>

      <div class="form-group form-button">
              <input type="submit" name="simpan" class="form-submit" value="Simpan" style="float: right;" />
      </div><br>

    </form>
              
    <?= form_open_multipart('pelamar/upload_sertifikat')?>
      <div class="input-group" style="padding-top: 15px;">
        <div class="custom-file">
          <input type="file" class="custom-file-input" value="sertifikat" name="sertifikat">
          <label class="custom-file-label">Upload Sertifikat</label>
        </div>
      </div>

      <div class="form-group form-button">
          <input type="submit" name="simpan" class="form-submit" value="Simpan" style="float: right;" />
      </div><br>
    </form>

    <?= form_open_multipart('pelamar/upload_ktp')?>
      <div class="input-group" style="padding-top: 15px;">
        <div class="custom-file">
          <input type="file" class="custom-file-input" value="ktp" name="ktp">
          <label class="custom-file-label" for="inputGroupFile04">Upload KTP</label>
        </div>
      </div>

      <div class="form-group form-button">
        <input type="submit" name="simpan" class="form-submit" value="Simpan" style="float: right;" />
      </div><br>
    </form>
  </div>


