

<!-- panel pendidikan -->
  <div>
    <div class="container fluid">
      <h2 style="padding-top: 20px; text-align: center;"><i class="icofont-thief"></i>Histori Pendidikan</h2>
      <div class="pendidikan">
        <!-- isi -->
        <form method="GET">
          <div>
            <a class="btn btn-warning" style="margin-bottom: 10px;" href="<?= base_url('data_didik'); ?>"/>
              <i class="fa fa-plus-square"></i> Tambah Data
            </a>
          </div>
          
          <table class="table">
              <thead style="background-color: #C966FA; text-align: center;">
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Pendidikan</th>
                  <th scope="col">Instansi</th>
                  <th scope="col">Jurusan</th>
                  <th scope="col">Ijazah</th>
                  <th scope="col">Terhitung Mulai Tanggal</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              
              <tbody style="text-align: center;">
                  <tr>
                    <th scope="row">1</th>
                    <td>SMK</td>
                    <td>SMK Negeri 1 Gorontalo</td>
                    <td>RPL</td>
                    <td>Ijazah</td>
                    <td>2021</td>
                    <td><!-- <a href="#" class="btn btn-primary"><i class="fa fa-eye"></i></a> -->
                        <a href="#" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>
                  </tr>
              </tbody>
          </table>
        </form>
        <!-- /isi -->
      </div>
    </div>
  </div>
