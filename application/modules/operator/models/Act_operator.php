<?php
defined('BASEPATH') or exit('no direct script  access allowed');

/**
 * 
 */
class Act_operator extends CI_Model
{

    function ambildata()
    {
        $query = $this->db->query('select a.kd, a.kd_user, b.nama ,  c.jenis_upload from t_upload_berkas a left join t_perusahaan b on md5(b.npwp) = a.kd_user left join t_jenis_upload c on c.id = a.id_jenis_upload left join t_verifikasi d on d.kd_upload = a.kd left join t_biodata bd on md5(bd.nik) = a.kd_user left join t_user e on e.kd = a.kd_user left join v_user vu on vu.kd = a.kd_user Where a.kd NOT IN(SELECT kd_upload FROM t_verifikasi) AND vu.id_role= 2 ORDER by a.kd_user , a.id_jenis_upload');

        return $query->result();
    }

    function ambildata2()
    {
        $query = $this->db->query('select a.kd, a.kd_user, b.nama ,  c.jenis_upload from t_upload_berkas a left join t_biodata b on md5(b.nik) = a.kd_user left join t_jenis_upload c on c.id = a.id_jenis_upload left join t_verifikasi d on d.kd_upload = a.kd left join t_biodata bd on md5(bd.nik) = a.kd_user left join t_user e on e.kd = a.kd_user left join v_user vu on vu.kd = a.kd_user Where a.kd NOT IN(SELECT kd_upload FROM t_verifikasi) AND vu.id_role= 1 ORDER by a.kd_user , a.id_jenis_upload');

        return $query->result();
    }

    function ambildata3()
    {
        $query = $this->db->query('select bd.nama , e.email from t_upload_berkas a left join t_perusahaan b on md5(b.npwp) = a.kd_user left join t_jenis_upload c on c.id = a.id_jenis_upload left join t_verifikasi d on d.kd_upload = a.kd left join t_biodata bd on md5(bd.nik) = a.kd_user left join t_user e on e.kd = a.kd_user left join v_user vu on vu.kd = a.kd_user Where a.kd NOT IN(SELECT kd_upload FROM t_verifikasi) AND vu.id_role= 1 ORDER by a.kd_user , a.id_jenis_upload');

        return $query->result();
    }

    function get_ktp($kduser)
    {

        return $this->db->query('Select url from t_upload_berkas where kd = "' . $kduser . '"');
    }

    function simpan($data)
    {

        $this->db->insert('t_verifikasi', $data);
    }
}
