<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: rgb(53,3,63);">
  <a href="#" class="brand-link" style="background-color: #5f4591;">

    <img src=" assets/img/icontab.png" height="40px" width="40px" style="margin-left: 10px;">

    <span class="brand-text font-weight-light">OPERATOR</span>
  </a>
  <div class="sidebar">

    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?= base_url('assets/'); ?>dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block " style="color: #ffffff;">YOUR NAME</a>
      </div>
    </div>



    <ul class=" nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fa fa-check-square "></i>
          <p style=" color: #ffffff;">
            VERIFIKASI UPLOAD
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="<?= base_url('index') ?>" class="nav-link">
              <i class="fa fa-building nav-icon"></i>
              <p style="color: #ffffff;">Perusahaan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('index2') ?>" class="nav-link">
              <i class="fas fa-user nav-icon"></i>
              <p style="color: #ffffff;">Pelamar</p>
            </a>
          </li>

        </ul>

      </li>

      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-edit "></i>
          <p style="color: #ffffff;">
            MANAGEMENT USERS
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="<?= base_url('index3') ?>" class="nav-link">
              <i class="fas fa-building nav-icon"></i>
              <p style="color: #ffffff;">Perusahaan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../../index2.html" class="nav-link">
              <i class="fas fa-user nav-icon"></i>
              <p style="color: #ffffff;">Pelamar</p>
            </a>
          </li>

        </ul>

      </li>

    </ul>




  </div>

</aside>