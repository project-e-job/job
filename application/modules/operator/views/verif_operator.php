<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-8">
        <div class="col-sm-6">
          <h1>VERIFIKASI</h1>
        </div>

      </div>
    </div><!-- /.container-fluid -->
  </section>
  <div class="content-body">


    <section class="content">
      <div class="row">
        <div class="col-md-8">
          <div class="card card-primary">
            <div class="card-header " style="background-color: #8a56ac; ">
              <h3 class="card-title">KTP</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body  ">
              <div class="form-group">
                <img src="http://192.168.108.11:800/jobpair/<?= $ktp ?>" class="img-fluid mx-auto " alt="white sample" style=" width : 800px; height: 400px;" />
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

        <div class="col-md-4">
          <form action="s_verif" method="post">
            <div class="card card-secondary">
              <div class="card-header" style=" background-color: #8a56ac;">
                <h3 class="card-title">Verifikasi</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>

              <div class="card-body">


                <div class="form-group">
                  <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">NIK</label>
                    <div class="col-sm-10">

                      <input type="hidden" name="kd_upload" value="<?= $kd_upload ?>">
                      <input type="text" class="form-control" name="nilai">

                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col">
                      <button type="submit" class="btn btn-success float-right">Tambah</button>
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.card-body -->

          </form>
        </div>
        <!-- /.card -->


      </div>

  </div>

  </section>
  <!-- /.content -->
</div>





</div>
<!-- Main content -->

<!-- <footer class="content-footer mx-auto my-auto" style=" background-color: 8a56ac; height :100px; ">
  <div class="float-right d-none d-block ">
    <b>Version</b> 3.1.0
  </div>
  <strong>
    Copyright &copy; 2014-2021 <a href="https://Poligon.ac.id">POLIGON.ac.id</a>.
  </strong> All rights reserved.
</footer> -->