<div class="content-wrapper">


    <div class="content-header">
        <div class="container-fluid">
            <div class="row-sm-5">
                <div class="col-md-3 mx-auto">
                    <h3>
                        <center>Tabel Upload</center>
                    </h3>
                </div>

            </div>
        </div>


    </div>


    <div class="content-body">

        <div class="container-fluid ">
            <div class="col-md-10 mx-auto  ">
                <?php
                if ($dataopr != NULL) {
                ?>

                    <table class="table table-bordered">
                        <thead>
                            <tr style="background-color: #8a56ac;">

                                <th scope="col">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">NPWP</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php



                            $count = 0;
                            $nm = '';
                            $list = '';
                            foreach ($dataopr  as $row) {
                                if ($nm != $row->nama) {
                                    if ($count > 0) {
                            ?>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Daftar Verifikasi
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <?php echo $list; ?>
                                                </div>
                                            </div>
                                        </td>
                                        </tr>
                                    <?php
                                    }
                                    $count = $count + 1;
                                    $list = '';
                                    $nm = $row->nama;

                                    ?>

                                    <tr>

                                        <td><?php echo $count ?> </td>
                                        <td> <?php echo $row->nama ?></td>
                                        <td><?php echo $row->jenis_upload ?></td>

                                <?php
                                    $ud = base_url('verif?kd=' . $row->kd);
                                    $list = $list . ' <a class="dropdown-item" href="' . $ud . '">' . $row->jenis_upload . '</a>';
                                } else {
                                    $ud = base_url('verif?kd=' . $row->kd);
                                    $list = $list . ' <a class="dropdown-item" href="' . $ud . '">' . $row->jenis_upload . '</a>';
                                }
                            }
                                ?>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Daftar Verifikasi
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <?php echo $list; ?>
                                        </div>
                                    </div>
                                </td>
                                    </tr>
                        </tbody>
                    </table>
                <?php } else  echo " <h1><center>Data Kosong</center></h1>"


                ?>
            </div>

        </div>



    </div>


</div>