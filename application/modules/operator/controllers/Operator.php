<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Operator extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Act_operator');
        $this->kdu = '';
    }
    public function index()
    {
        $recordopr = $this->Act_operator->ambildata();
        $data['dataopr'] =  $recordopr;
        $data['role'] = '3';
        $data['title'] = 'Halaman Dashboard';
        $data['content_page'] = 'operator/operator_view';
        $data['menu'] = 'operator/d_sidebar';
        $this->template->loadTemplate($data);
    }
    public function index2()
    {
        $recordopr = $this->Act_operator->ambildata2();
        $data['dataopr'] =  $recordopr;
        $data['role'] = '3';
        $data['title'] = 'Halaman Dashboard';
        $data['content_page'] = 'operator/operator_view2';
        $data['menu'] = 'operator/d_sidebar';
        $this->template->loadTemplate($data);
    }

    public function index3()
    {
        $recordopr = $this->Act_operator->ambildata3();
        $data['dataopr'] =  $recordopr;
        $data['role'] = '3';
        $data['title'] = 'Halaman Dashboard';
        $data['content_page'] = 'operator/user_view';
        $data['menu'] = 'operator/d_sidebar';
        $this->template->loadTemplate($data);
    }
    public function verif()
    {
        $data['kd_upload'] = $_GET['kd'];
        $data['role'] = '3';
        $data['ktp'] = $this->Act_operator->get_ktp($data['kd_upload'])->row()->url;
        $data['title'] = 'Halaman Dashboard';
        $data['content_page'] = 'operator/verif_operator';
        $data['menu'] = 'operator/d_sidebar';
        $this->template->loadTemplate($data);
    }

    public function simpandata()
    {



        $data = array(


            'kd_upload' => $_POST['kd_upload'],
            'kd_user'  => $this->session->userdata('kd'),
            'nilai'    => $this->input->post("nilai")
        );



        $this->Act_operator->simpan($data);

        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success! data berhasil disimpan didatabase.
                                                </div>');
        redirect('operator');
    }
}
