<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $title; ?></title>
  <link href="assets/img/default_ic.png" rel="icon">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>dist/css/custom_style.css">
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>dist/css/footer_style.css">

  <!-- Vendor CSS Files -->
</head>

<!-- nav header & content -->
<body class="hold-transition layout-top-nav layout-footer-fixed">
<div class="wrapper">
  <!-- Main Header Navbar -->
<nav id="header" class="main-header navbar navbar-expand-md navbar-dark navbar-custom">
    <div class="container">

      <div id="logo" class="navbar-brand">
          <a href="landing_page"><img src="assets/img/logo_loop.png" alt="TOLOPANI" class="brand-image" alt="logo"></a>
      </div>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <ul class="navbar-custom navbar-nav ml-auto">
          <li><a class="nav-item" href="login">Login</a></li>
            <li class="dropdown"><a href="#"><span>Registrasi</span> <i class="bi bi-chevron-down"></i></a>
              <ul>
                <li><a href="regis_pelamar">Pelamar</a></li>
                <li><a href="regis_perusahaan">Perusahaan</a></li>
              </ul>
          </li>
        </ul>
      </div>

    </div>
</nav>
<!-- /.Main Header navbar -->
<!-- ./HEADER -->
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid ">
        <!-- CONTENT -->
        <?php $this->load->view($content_page); ?>
        <!-- ./CONTENT -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
<!-- FOOTER -->
<!-- Main Footer -->
  <footer class="main-footer" id="footer">
    <div class="d-none d-sm-inline d-sm-block">
    <marquee>
    <strong>Hak Cipta &copy; 2021 <a href="http://ti.poligon.ac.id" target="_blank">Tim IT POLTEKGO</a>.</strong> All rights reserved.
    </marquee>
    </div>
 </footer>
<!-- ./footer -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<!-- Template Main JS File -->
<script src="<?= base_url('assets/'); ?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/'); ?>dist/js/adminlte.min.js"></script>
</body>
</html>