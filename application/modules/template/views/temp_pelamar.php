<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $title; ?></title>
  <link href="assets/img/default_ic.png" rel="icon">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">

  <link rel="stylesheet" href="assets/p_css/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="assets/p_css/css/style1.css">

    <link rel="stylesheet" type="text/css" href="assets/p_css/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script type="text/javascript" src="assets/p_css/js/jquery.js"></script>
    <script type="text/javascript" src="assets/p_css/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/p_css/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="assets/p_css/js/jquery.ui.js"></script>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
          <!-- jQuery UI -->
          <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

  <!-- Vendor CSS Files -->
</head>

<body>
       <div style="background-color: #610655;">
        <img style="padding: 20px;" src="assets/p_css/images/iconweb.png" width="150" height="150">
       
       <ul class="nav nav-tabs">&nbsp;&nbsp;&nbsp;
         <li><a style="color: #4B3869;" href="<?= base_url('profile'); ?>">Profile</a></li>&nbsp;&nbsp;&nbsp;
         <li><a style="color: #4B3869;" href="<?= base_url('pendidikan'); ?>">Pendidikan</a></li>&nbsp;&nbsp;&nbsp;
         <li><a style="color: #4B3869;" href="<?= base_url('joblist'); ?>">Job List</a></li>&nbsp;&nbsp;&nbsp;
         <!-- <li><a style="color: #4B3869;" href="<?= base_url('kuisioner'); ?>">Kuisioner</a></li> -->
         <li><a href="<?= base_url('logout') ?>" style="float: right; color: #4B3869; text-decoration: none;">Logout</a></li>
       </ul>
       </div>


       <?php $this->load->view($content_page); ?>


       <footer style="position: fixed; bottom: 0; width: 100%; background-color: #610655; color: #fff;">
          <marquee><strong>Copyright &copy; 2021 <a href="http://ti.poligon.ac.id">Tim IT POLTEKGO</a>.</strong> All rights reserved.</marquee>
        </footer>
  <script src="<?= base_url('assets/'); ?>plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
 </body>
</html>
</html>