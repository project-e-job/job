<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Template extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function loadTemplate($data = NULL)
	{
		switch ($data['role']) {
			case '1':
				$this->load->view("template/temp_pelamar", $data);
				break;
			case '2':
				$this->load->view("template/temp_perusahaan", $data);
				break;
			case '3':
				$this->load->model('Act_operator');
				$this->load->view('navbar_operator');
				$this->load->view($data['menu']);
				$this->load->view("template/temp_operator", $data);
				break;

			default:
				$this->load->view('template/temp_global', $data);
				break;
		}
	}

}
