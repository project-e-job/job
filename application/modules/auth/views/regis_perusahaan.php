<div class="row">
  <!-- Register-box -->
  <div class="mt-5 mx-auto">
  <!-- Card -->
  <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <a href="#" class="h1"><b>Registrasi</b> Perusahaan</a>
      </div>
    <div class="card-body">
      <p class="login-box-msg">Segera daftarkan perusahaan untuk dapat merekrut karyawan</p>
      <form action="regis_perusahaan" method="post">
      <?= form_error('nama', '<small class="text-danger pl-3">', '</small>') ?>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-building"></span>
            </div>
          </div>
          <input type="text" class="form-control" placeholder="Nama Perusahaan" name="nama" value="<?= set_value('nama'); ?>">
        </div>

        <?= form_error('alamat', '<small class="text-danger pl-3">', '</small>') ?>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fw fa-map-marker-alt"></span>
            </div>
          </div>
          <input type="text" class="form-control" placeholder="Alamat" name="alamat" value="<?= set_value('alamat'); ?>">
        </div>

        <?= form_error('telp', '<small class="text-danger pl-3">', '</small>') ?>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone-alt"></span>
            </div>
          </div>
          <input type="text" class="form-control" placeholder="No. Telepon" name="telp" value="<?= set_value('telp'); ?>">
        </div>

        <?= form_error('npwp', '<small class="text-danger pl-3">', '</small>') ?>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-feather-alt"></span>
            </div>
          </div>
          <input type="text" class="form-control" placeholder="NPWP" name="npwp" value="<?= set_value('npwp'); ?>">
        </div>

        <?= form_error('email', '<small class="text-danger pl-3">', '</small>') ?>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          <input type="email" class="form-control" placeholder="Email" name="email" value="<?= set_value('email'); ?>">
        </div>

        <?= form_error('password', '<small class="text-danger pl-3">', '</small>') ?>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <input type="password" class="form-control" placeholder="Password" name="password">
        </div>

        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <input type="password" class="form-control" placeholder="Ketik ulang password" name="password2">
        </div>

        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree" onchange="document.getElementById('regis').disabled = !this.checked;">
              <label for="agreeTerms">
              Data yang anda masukkan telah benar.
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block" id="regis" disabled>Daftar</button>
          </div>
          <!-- /.col -->
        </div>

      </form>
      <!-- /.form-box -->
    </div>
  </div>
  <!-- /.card -->
</div>
<!-- /.register-box -->
</div>
<!-- /.row -->