    <?= $this->session->flashdata('message'); ?>
  <div class="row">
    <div class="login-box mt-5 mx-auto">
      <div class="card card-outline card-primary">
        <div class="card-header text-center">
          <div id="logo" class="navbar-brand">
            <a href="index.html"><img src="assets/img/logo_heading.png" class="brand-image" style="width:200px;" alt="kadin"></a>
          </div>
        </div>
        <div class="card-body">
          <p class="login-box-msg">Masukkan email dan password</p>
          <?= $this->session->flashdata('message_wrong'); ?>
          <form action="<?= base_url('/auth'); ?>" method="post">
            <?= form_error('nama', '<small class="text-danger pl-3">', '</small>') ?>
            <div class="input-group mb-3">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
              <input type="email" class="form-control" placeholder="Email" name="email">
            </div>
            <?= form_error('password', '<small class="text-danger pl-3">', '</small>') ?>
            <div class="input-group mb-3">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
              <input type="password" class="form-control" placeholder="Password" name="password">
            </div>
            
            <div class="row">
              <div class="col-8">
                <div class="icheck-primary">
                  <input type="checkbox" id="remember">
                  <label for="remember">
                    Simpan Akun?
                  </label>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">Masuk</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
          <p class="mb-1">
            <a href="#lupa_password" data-toggle="modal" data-target="#modal-default">Lupa password?</a>
          </p>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <div class="row">
          <div class="text-center mx-auto mt-2 p-1">
          <p class="mb-0">Supported by :</p>
            <div id="logo" class="navbar-brand mr-3">
              <a href="https://kadingorontalo.id/" data-toggle="tooltip" data-placement="bottom" title="Kamar Dagang Industri Provinsi Gorontalo" target="_blank"><img src="assets/img/logo_kadin.png" class="brand-image img-size-50" alt="kadin"></a>
            </div>
            <div id="logo" class="navbar-brand ml-3">
              <a href="https://poligon.ac.id/" data-toggle="tooltip" data-placement="bottom" title="Politeknik Gorontalo" target="_blank"><img src="assets/img/logo_campus.jpg" class="brand-image img-size-50" alt="poltekgo"></a>
            </div>
          <hr>
          </div>
        </div>
    </div> <!-- /.login-box -->
  </div>
  <!-- /.row -->

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-purple">
          <h4 class="modal-title">Kirim Link Reset Password</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="auth/recovery" method="post">
            <div class="input-group mb-3">
              <input type="email" class="form-control" placeholder="Email Perusahaan" >
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="far fa-envelope"></span>
                </div>
              </div>
            </div>
            <div class="row justify-content-between">
              <div class="col-4">
                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Batal</button>
              </div>
              <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">Kirim</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<!-- /.modal -->
<script>
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  }) 
</script>