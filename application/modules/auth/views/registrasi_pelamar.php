<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registrasi Pelamar</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="<?= base_url('assets/p_css/css/style.css')?>">

    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/p_css/css/bootstrap.css')?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</head>
<body>

    <div class="banner">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="<?= base_url('assets/p_css/images/banner.png')?>" class="d-block w-100" alt="gambar1">
            </div>
            <div class="carousel-item">
              <img src="<?= base_url('assets/p_css/images/banner2_tolopani.png')?>" class="d-block w-100" alt="gambar2">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    </div>

    <div class="main">

        <!-- Sign up form -->
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <div class="signup-form">
                        <h2 class="form-title">Registrasi Akun</h2>

                        <form method="POST" action="<?= base_url('auth/registration1'); ?>" class="register-form">
                            <div class="form-group">
                                <label for="name"><i class="fa fa-id-card" aria-hidden="true"></i></label>
                                <input type="number" name="nik" placeholder="NIK"/>
                                <?= form_error('nik','<small class="text-danger pl3">', '</small'); ?>
                            </div>
                            <div class="form-group">
                                <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="nama" placeholder="Nama Lengkap"/>
                                <?= form_error('nama','<small class="text-danger pl3">', '</small'); ?>
                            </div>
                            <div class="form-group">
                                <label for="hp"><i class="fa fa-phone" aria-hidden="true"></i></i></label>
                                <input type="number" name="no_hp" placeholder="Nomor Handphone"/>
                                <?= form_error('no_hp','<small class="text-danger pl3">', '</small'); ?>
                            </div>
                            <div class="form-group">
                                <label for="hp"><i class="fa fa-map-marker" aria-hidden="true"></i></label>
                                <input type="text" name="alamat" placeholder="Alamat"/>
                                <?= form_error('alamat','<small class="text-danger pl3">', '</small'); ?>
                            </div>
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" name="email" placeholder="Email"/>
                                <?= form_error('email','<small class="text-danger pl3">', '</small'); ?>
                            </div>
                            <div class="form-group">
                                <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="pass" placeholder="Password"/>
                                <?= form_error('pass','<small class="text-danger pl3">', '</small'); ?>
                            </div>
                            <div class="form-group">
                                <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="re_pass" placeholder="Re-password"/>
                                <?= form_error('re_pass','<small class="text-danger pl3">', '</small'); ?>
                            </div>
                            
                            <div class="form-group form-button">
                                <input type="submit" name="regis" id="signup" class="form-submit" value="Register"/>
                                <a href="#" class="signup-image-link">Sudah Punya Akun?</a>
                            </div>
                        </form>

                    </div>

                    <div class="signup-image">
                        <figure><img width="300" height="400" src="<?= base_url('assets/p_css/images/iconsplash.png')?>" alt="sing up image"></figure>
                        <!-- <img width="170" height="170" src="<?= base_url('assets/p_css/images/kadinlogo.png')?>" alt="sing up image">
                        <img width="80" height="80" src="<?= base_url('assets/p_css/images/poltekgo.png')?>" alt="sing up image"> -->
                        <h1 align="center" style="color: purple;">Pencarian Lowongan Kerja Se Gorontalo</h1>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <footer style="position: fixed; bottom: 0; width: 100%; background-color: #610655; color: #fff;">
       <marquee><strong>Copyright &copy; 2021 <a href="http://ti.poligon.ac.id">Tim IT POLTEKGO</a>.</strong> All rights reserved.</marquee>
    </footer>

    <!-- JS -->
    <script src="<?= base_url('assets/p_css/vendor/jquery/jquery.min.js')?>"></script>
    <script src="<?= base_url('assets/p_css/js/main.js')?>"></script>
    
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>