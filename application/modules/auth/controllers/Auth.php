<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        $this->load->model('m_auth_2');
    }

    public function index()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Halaman Login';
            $data['role'] = NULL;
            $data['content_page'] = "auth/login";
            echo Modules::run('template/loadTemplate', $data);
        } else {
            $this->_login();
        }
    }

    private function _login()

    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $data = array(
            'email' => $email,
            'password' => $password
        );

        $encode = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://tolopani.web.id:800/auth/login");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encode);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $decode = json_decode($result, true);
        if (!$decode['error']) {
            if ($decode['code'] == 0) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Akun ini belum diregistrasi.</div>');
                redirect('login');
            } elseif ($decode['code'] == 1) {
                $this->session->set_flashdata('message_wrong', '<div class="alert alert-danger" role="alert">Password anda   salah.</div>');
                redirect('login');
            } elseif ($decode['code'] == 2) {
                if (!$decode['error']) {
                    $data = [
                        'kd' => $decode['data']['kd'],
                        'id_role' => $decode['data']['id_role']
                    ];
                    switch ($decode['data']['id_role']) {
                        case 1:
                            $this->session->set_userdata($data);
                            redirect('pelamar');
                            break;

                        case 2:
                            $this->session->set_userdata($data);
                            redirect('perusahaan');
                            break;

                        case 3:
                            $this->session->set_userdata($data);
                            redirect('operator');
                            break;

                        case 4:
                            $this->session->set_userdata($data);
                            redirect('admin');
                            break;

                        default:
                            redirect('login');
                            break;
                    }
                }
            }
        }
    }

    public function registration1(){
        $nik = $this->input->post('nik');
        $nama = $this->input->post('nama');
        $no_hp = $this->input->post('no_hp');
        $alamat = $this->input->post('alamat');
        $email = $this->input->post('email');
        $password = $this->input->post('pass');
        $re_pass = $this->input->post('re_pass');

        $data = array (
            'email' => $email,
            'password' => $password,
            'nik' => $nik,
            'nama' => $nama,
            'alamat' => $alamat,
            'no_hp' => $no_hp,
        );

        $encode = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://192.168.108.11:800/jobpair/auth/register");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encode);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $decode = json_decode($result, true);

        if (!$decode["error"]) {
            if ($decode["code"]==1) {
                $this->session->set_flashdata('message', '<div class="alert alert-Success" role="alert">Email ini berhasil diregistrasi.</div>');
                redirect('login');
            }elseif ($decode["code"]==0) {
                $this->session->set_flashdata('message', '<div class="alert alert-red" role="alert">Email ini sudah aktif</div>');
                redirect('regis_pelamar');
            }
        }
    }

    public function regis_pelamar()
    {
            $data['title'] = 'Halaman Registrasi Pelamar';
            $data['role'] = null;
            $this->load->view("auth/registrasi_pelamar",$data);
    }



    public function registration_2()
    {
        $reg = $this->m_auth_2;
        $validation = $this->form_validation;

        $validation->set_rules($reg->rules());

        if ($validation->run() == false) {
            $data['title'] = 'Halaman Registrasi Perusahaan';
            $data['role'] = NULL;
            $data['content_page'] = "auth/regis_perusahaan";
            echo Modules::run('template/loadTemplate', $data);
        } else {
            $reg->save();

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Akun telah berhasil dibuat!, silakan login.</div>');
            redirect('login');
        }
    }

    public function logout()
    {
        unset(
            $_SESSION['kd'],
            $_SESSION['id_role']
        );

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Anda keluar aplikasi!</div>');
        redirect('login');
    }
}
