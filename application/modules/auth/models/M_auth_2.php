<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_auth_2 extends MY_Controller
{
    private $_Tperusahaan = "t_perusahaan";
    private $_Tuser = "t_user";

    public function rules()
    {
        return $rules =  array(
            array('field' => 'npwp',
             'label' => 'NPWP',
             'rules' => 'required|trim',
             'errors' => array(
                'required' => 'NPWP harus diisi.'
                ),
            ),

            array('field' => 'nama',
             'label' => 'Nama',
             'rules' => 'required|trim',
             'errors' => array(
                'required' => 'Nama perusahaan harus diisi.'
                ),
            ),

            array('field' => 'alamat',
             'label' => 'Alamat',
             'rules' => 'required|trim',
             'errors' => array(
                'required' => 'Alamat harus diisi.'
                ),
            ),

            array('field' => 'telp',
             'label' => 'Telp',
             'rules' => 'required|trim',
             'errors' => array(
                'required' => 'No. telpon harus diisi.'
                ),
            ),

            array('field' => 'email',
                'label' => 'Email',
                'rules' => 'required|is_unique[t_user.email]', 
                'errors' => array(
                'required' => 'email harus diisi.',
                'is_unique' => 'email ini sudah diregistrasi!'
                ),
             ),

            array('field' => 'password',
                'label' => 'Password',
                'rules' => 'required|trim|min_length[7]|matches[password2]',
                'errors' => array(
                    'required' => 'Masukkan Password.',
                    'matches' => 'Password tidak cocok!',
                    'min_length' => 'Password terlalu pendek, minimal 8 digit.'
                ),
            ),

            array('field' => 'password2',
                    'label' => 'Password',
                    'rules' => 'required|trim|matches[password]',
                    'errors' => array(
                    'required' => 'password ketik kembali.'
                    ),
            )
        );
    }

    public function save()
    {
        $post = $this->input->post();
        
        $data0 = [
            'npwp' => htmlspecialchars($post["npwp"]),
            'nama' => htmlspecialchars( $post["nama"]),
            'alamat' => $post["alamat"],
            'no_telp' => $post["telp"]
        ];
        $this->db->insert($this->_Tperusahaan, $data0);
        
        $data1 = [
            'kd' => md5($data0['npwp']),
            'email' => $post["email"],
            'password' => md5($post["password"]),
            'status' => '1'
        ];
        $this->db->insert($this->_Tuser, $data1);
        
    }

}