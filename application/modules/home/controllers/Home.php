<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
            $data['title'] = 'Halaman Utama';
            $data['role'] = NULL;
            $data['content_page'] = "auth/login";
    }