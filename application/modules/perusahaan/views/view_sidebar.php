<div class="container">
   <div class="row">
      <div class='col-sm-6'>
         <div class="form-group">
            <div class='input-group date' id='datetimepicker1'>
               <input type='text' class="form-control" />
               <span class="input-group-addon">
               <span class="glyphicon glyphicon-calendar"></span>
               </span>
            </div>
         </div>
      </div>
      <input class="datepicker" data-date-format="mm/dd/yyyy">
      <script type="text/javascript">
         $(function () {
             $('#datetimepicker1').datetimepicker();
         });
        $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-3d'
        });

      </script>
   </div>
</div>

<div class="input-group date" data-provide="datepicker">
    <input type="text" class="form-control">
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>