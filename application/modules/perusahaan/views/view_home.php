<div class="row">
  <div class="col-12">
        <div class="callout callout-info">
          <h5><i class="fas fa-info"></i> Note:</h5>
          Akun Perusahaan dalam tahap verifikasi oleh kadin, fitur untuk membuka lowongan dinonaktifkan.
        </div>
  </div>
</div>
<!-- ./row -->

<div class="row">
  <div class="col-sm-6">
  <!-- Donut chart -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            <i class="far fa-chart-bar"></i>
            Donut Chart
          </h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
          <div class="card-body">
            <div id="donut-chart" style="height: 300px;"></div>
          </div>
        <!-- /.card-body-->
      </div>
  <!-- /.card -->
  </div>

  <div class="col-lg-3 col-sm-6">
    <!-- small card -->
    <div class="small-box bg-danger">
      <div class="inner">
        <h3>65</h3>
        <p>Unique Visitors</p>
      </div>
      <div class="icon">
        <i class="fas fa-chart-pie"></i>
      </div>
      <a href="#" class="small-box-footer">
        More info <i class="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <!-- ./col -->
</div>
<!-- ./row -->