<div class="row">
    <div class="col-md-4">

      <!-- Profile Image -->
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                  src="assets/img/default.png"
                  alt="Logo Perushaan">
          </div>

          <h3 class="profile-username text-center">$nama_perusahaan</h3>

          <p class="text-muted text-center">$npwp</p>

          <a href="#" class="btn btn-primary btn-block"><b>Ubah Logo</b></a>

          <hr>

          <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>

          <p class="text-muted">$alamat</p>

          <hr>

          <strong><i class="far fa-file-alt mr-1"></i> Latar Belakang</strong>

          <p class="text-muted">$latar</p>

          <hr>

          <strong><i class="fas fa-book mr-1"></i> Visi Misi</strong>

          <p class="text-muted">
            $deskripsi
          </p>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->

    <div class="col-md-8">
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link active" href="#Surat" data-toggle="tab">Upload Surat</a></li>
            <li class="nav-item"><a class="nav-link" href="#edit" data-toggle="tab">Edit Profil</a></li>
            <li class="nav-item"><a class="nav-link" href="#visi" data-toggle="tab">Visi Misi</a></li>
          </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="tab-content">
            <div class="active tab-pane" id="Surat">
              <!-- <form class="form-horizontal" method="get" action="perusahaan/upload_surat" enctype="multipart/form-data"> -->
                <div class="form-group row">
                  <label for="InputFile1" class="col-sm-3 col-form-label">NPWP</label>
                  <div class="col-sm-8">
                    <div class="custom-file">
                      <?= form_open_multipart('perusahaan/upload_surat'); ?>
                      <!-- <input type="file" class="custom-file-input" id="InputFile1" name="surat"> -->
                      <input type="file" name="surat" id="InputFile1" class="form-control">
                      <label class="custom-file-label">Format Gambar</label>
                    </div>
                  </div>
                </div>
                <!-- <div class="form-group row">
                  <label for="InputFile2" class="col-sm-3 col-form-label">Surat Izin Usaha</label>
                  <div class="col-sm-8">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="InputFile2"  name="upload2">
                      <label class="custom-file-label">Format gambar</label>
                    </div>
                  </div>
                </div> -->
                <div class="form-group row">
                  <div class="offset-sm-2 col-sm-4">
                    <button type="submit" class="btn btn-outline-info btn-block btn-flat"><i class="fa fa-book"></i> Upload</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="edit">
              <form class="form-horizontal" action="edit_profil">
                <div class="form-group row">
                  <label for="inputName" class="col-sm-2 col-form-label">Nama </label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputName" placeholder="Nama Perusahaan">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" placeholder="Email Perusahaan">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputAlamat" placeholder="Alamat Perusahaan">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputNomor" class="col-sm-2 col-form-label">No. Telp</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputNomor" placeholder="Nomor Telepon">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="offset-sm-2 col-sm-4">
                    <button type="submit" class="btn btn-outline-warning btn-block btn-flat"><i class="fa fa-edit"></i>Simpan</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="visi">
              <form class="form-horizontal" action="edit_visiMisi">
                <div class="form-group row">
                  <label for="inputName" class="col-sm-2 col-form-label">Visi</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputName" placeholder="Visi Perusahaan" name="visi">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Misi</label>
                  <div class="col-sm-8">
                        <button type="button" class="btn btn-outline-info btn-flat mb-2" onclick="addField()"><i class="fa fa-plus"></i> Klik untuk tambah</button>
                      <table id="newInput" cellpadding="5">
                    </table>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="offset-sm-2 col-sm-4">
                    <button type="submit" class="btn btn-outline-primary btn-flat">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div><!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<script>
  const table = document.getElementById("newInput"); 
  function createField(name) {
    let input         = document.createElement("input")
    input.type        = 'text'
    input.class       = 'form-control'
    input.name        = 'misi'
    input.placeholder = name
    return input
  }
  function addField() {
    let rowNumber = table.rows.length
    let row       = table.insertRow(rowNumber)
    row.insertCell(0).appendChild(createField('Misi Perusahaan'))
  }    
</script>