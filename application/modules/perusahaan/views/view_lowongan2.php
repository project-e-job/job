

   <div class="content-wrapper">
       <div class="col-md-8">
           <div class="card card-purple">
               <div class="card-header">
                   <h3 class="card-title">Upload Lowongan Kerja!</h3>
               </div>
               <div class="card-body">
                   <div class="form-group">
                       <label for="exampleInputEmail1">Npwp Perusahaan:</label>
                       <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukkan NPWP" name="npwp">
                   </div>
                   <div class="form-group">
                       <label for="exampleInputEmail2">Formasi Kerja:</label>
                       <input type="text" class="form-control" id="exampleInputEmail2" placeholder="Masukkan Formasi Kerja" name="formasi">
                   </div>


                   <div class="form-group">
                       <label for="exampleInputEmail1">Deskripsi :</label>
                       <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Deskripsi Pencarian Kerja" name="deskripsi">
                   </div>
                   <!-- Date -->
                   <div class="form-group">
                       <label>Sejak Tanggal :</label>
                       <div class="input-group date" id="reservationdate" data-target-input="nearest">
                           <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="from"/>
                           <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                               <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                           </div>
                       </div>
                   </div>

                   <div class="form-group">
                       <label>Sampai Tanggal :</label>
                       <div class="input-group date" id="reservationdate2" data-target-input="nearest">
                           <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate2" name="to" />
                           <div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
                               <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                           </div>
                       </div>
                   </div>
                   <a class="btn btn-app col-3 btn-outline-primary float-right elevation-2">
                       <i class="fas fa-save"></i> Kirim
                   </a>
                   <!-- Date and time -->

                   <!-- /.form group -->
                   <!-- Date range -->

                   <!-- /.form group -->
               </div>
               <!-- /.card-body -->
           </div>
           <!-- /.card -->
           <div class="col-md-7 col-sm-7">
               <div class="info-box bg-gradient-warning">
                   <span class="info-box-icon"><i class="far fa-calendar-alt"></i></span>

                   <div class="info-box-content">
                       <span class="info-box-text">Agenda Lowongan Kerja</span>
                       <span class="info-box-number">$jumlah</span>
                       <span class="progress-description">
                           Jumlah Loker yang disediakan
                       </span>
                   </div>
                   <!-- /.info-box-content -->
               </div>
               <!-- /.info-box -->
           </div>

           <!-- /.card -->
       </div>
   </div>



   <script src="<?= base_url('assets/'); ?>plugins/jquery/jquery.min.js"></script>
   <!-- Bootstrap 4 -->
   <script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!-- Select2 -->
   <script src="<?= base_url('assets/'); ?>plugins/select2/js/select2.full.min.js"></script>
   <!-- Bootstrap4 Duallistbox -->
   <script src="<?= base_url('assets/'); ?>plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
   <!-- InputMask -->
   <script src="<?= base_url('assets/'); ?>plugins/moment/moment.min.js"></script>
   <script src="<?= base_url('assets/'); ?>plugins/inputmask/jquery.inputmask.min.js"></script>
   <!-- date-range-picker -->
   <script src="<?= base_url('assets/'); ?>plugins/daterangepicker/daterangepicker.js"></script>
   <!-- bootstrap color picker -->
   <script src="<?= base_url('assets/'); ?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
   <!-- Tempusdominus Bootstrap 4 -->
   <script src="<?= base_url('assets/'); ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
   <!-- Bootstrap Switch -->
   <script src="<?= base_url('assets/'); ?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
   <!-- BS-Stepper -->
   <script src="<?= base_url('assets/'); ?>plugins/bs-stepper/js/bs-stepper.min.js"></script>
   <!-- dropzonejs -->
   <script src="<?= base_url('assets/'); ?>plugins/dropzone/min/dropzone.min.js"></script>
   <!-- AdminLTE App -->
   <script src="<?= base_url('assets/'); ?>dist/js/adminlte.min.js"></script>
   <!-- AdminLTE for demo purposes -->
   <script src="<?= base_url('assets/'); ?>dist/js/demo.js"></script>
   <!-- Page specific script -->
   <script>
       $(function() {
           //Initialize Select2 Elements
           $('.select2').select2()

           //Initialize Select2 Elements
           $('.select2bs4').select2({
               theme: 'bootstrap4'
           })

           //Datemask dd/mm/yyyy
           $('#datemask').inputmask('dd/mm/yyyy', {
               'placeholder': 'dd/mm/yyyy'
           })
           //Datemask2 mm/dd/yyyy
           $('#datemask2').inputmask('mm/dd/yyyy', {
               'placeholder': 'mm/dd/yyyy'
           })
           //Money Euro
           $('[data-mask]').inputmask()

           //Date picker
           $('#reservationdate').datetimepicker({
               format: 'L'
           });

           $('#reservationdate2').datetimepicker({
               format: 'L'
           });


           //Date and time picker
           $('#reservationdatetime').datetimepicker({
               icons: {
                   time: 'far fa-clock'
               }
           });

           //Date range picker
           $('#reservation').daterangepicker()
           $('#reservation2').daterangepicker()
           //Date range picker with time picker
           $('#reservationtime').daterangepicker({
               timePicker: true,
               timePickerIncrement: 30,
               locale: {
                   format: 'MM/DD/YYYY hh:mm A'
               }
           })
           //Date range as a button
           $('#daterange-btn').daterangepicker({
                   ranges: {
                       'Today': [moment(), moment()],
                       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                       'This Month': [moment().startOf('month'), moment().endOf('month')],
                       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                   },
                   startDate: moment().subtract(29, 'days'),
                   endDate: moment()
               },
               function(start, end) {
                   $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
               }
           )

           //Timepicker
           $('#timepicker').datetimepicker({
               format: 'LT'
           })

           //Bootstrap Duallistbox
           $('.duallistbox').bootstrapDualListbox()

           //Colorpicker
           $('.my-colorpicker1').colorpicker()
           //color picker with addon
           $('.my-colorpicker2').colorpicker()

           $('.my-colorpicker2').on('colorpickerChange', function(event) {
               $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
           })

           $("input[data-bootstrap-switch]").each(function() {
               $(this).bootstrapSwitch('state', $(this).prop('checked'));
           })

       })
       // BS-Stepper Init
       document.addEventListener('DOMContentLoaded', function() {
           window.stepper = new Stepper(document.querySelector('.bs-stepper'))
       })

       // DropzoneJS Demo Code Start
       Dropzone.autoDiscover = false

       // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
       var previewNode = document.querySelector("#template")
       previewNode.id = ""
       var previewTemplate = previewNode.parentNode.innerHTML
       previewNode.parentNode.removeChild(previewNode)

       var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
           url: "/target-url", // Set the url
           thumbnailWidth: 80,
           thumbnailHeight: 80,
           parallelUploads: 20,
           previewTemplate: previewTemplate,
           autoQueue: false, // Make sure the files aren't queued until manually added
           previewsContainer: "#previews", // Define the container to display the previews
           clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
       })

       myDropzone.on("addedfile", function(file) {
           // Hookup the start button
           file.previewElement.querySelector(".start").onclick = function() {
               myDropzone.enqueueFile(file)
           }
       })

       // Update the total progress bar
       myDropzone.on("totaluploadprogress", function(progress) {
           document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
       })

       myDropzone.on("sending", function(file) {
           // Show the total progress bar when upload starts
           document.querySelector("#total-progress").style.opacity = "1"
           // And disable the start button
           file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
       })

       // Hide the total progress bar when nothing's uploading anymore
       myDropzone.on("queuecomplete", function(progress) {
           document.querySelector("#total-progress").style.opacity = "0"
       })

       // Setup the buttons for all transfers
       // The "add files" button doesn't need to be setup because the config
       // `clickable` has already been specified.
       document.querySelector("#actions .start").onclick = function() {
           myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
       }
       document.querySelector("#actions .cancel").onclick = function() {
           myDropzone.removeAllFiles(true)
       }
   </script>