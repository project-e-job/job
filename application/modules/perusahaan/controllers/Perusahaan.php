<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perusahaan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_perusahaan');
    }

    public function index()
    {
        // if ($_SESSION == NULL) {
        //     echo '<script>
        //     alert("You have to login to access this site");
        //     window.location.href="login";
        //     </script>';
        // } else {
        //     $valid = $this->M_perusahaan->getById($_SESSION['kd']);
        //     if ($valid == NULL) {
        //         echo '<script>
        //         alert("You dont have access here!");
        //         window.location.href="logout";
        //         </script>';
        //     } else {
                $data['title'] = 'Menu Utama | Dashboard Perusahaan';
                $data['content_page'] = 'perusahaan/view_home';
                $data['page'] = 'Utama';
                $data['menu'] = 'perusahaan/view_sidebar';
                $data['role'] = $_SESSION['id_role'];
                $data['data'] = $this->_getData();
                // print_r($data['nama']); die();
                echo Modules::run('template/loadTemplate', $data);
        //     }
        // }
    }

    private function _getData()
    {
        return $this->M_perusahaan->getById($_SESSION['kd']);
    }

    public function profil()
    {
        $data['role'] = '2';
        $data['title'] = 'Menu Profil | Dashboard Perusahaan';
        $data['content_page'] = 'perusahaan/view_profil';
        $data['page'] = 'Profil';
        $data['data'] = $this->_getData();
        echo Modules::run('template/loadTemplate', $data);
    }

    public function lowongan()
    {
        $data['role'] = '2';
        $data['title'] = 'Menu Lowongan | Dashboard Perusahaan';
        $data['content_page'] = 'perusahaan/view_lowongan2';
        $data['page'] = 'Lowongan';
        $data['data'] = $this->_getData();
        echo Modules::run('template/loadTemplate', $data);
    }

    public function upload_surat()
    {
        $kd_upload = uniqid($_SESSION['kd']);
        $config['file_name']            = $kd_upload;
        // $config['upload_path']          = './assets/img/';
        $config['upload_path']          = 'var/www/api/jobpair/';
        $config['allowed_types']        = 'jpg|png';
        $config['overwrite']            = TRUE;
        $config['max_size']             = 4096;
        
        $this->load->library('upload', $config);
        $this->upload->do_upload('surat');
                echo '<pre>';
                print_r($this->upload->data());
                echo '</pre>';
        $kd_img = $this->upload->data('full_path');
        $this->M_perusahaan->save_img($kd_img);
    }

    public function update_profil(){
        $post = $this->input->post();
        $this->M_perusahaan->up_profil($post);
    }

    
}